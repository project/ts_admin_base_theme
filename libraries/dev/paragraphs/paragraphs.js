/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./bundles/paragraphs/js/paragraphs.js":
/*!*********************************************!*\
  !*** ./bundles/paragraphs/js/paragraphs.js ***!
  \*********************************************/
/***/ (() => {

eval("(function ($, Drupal) {\n  // closure\n  'use strict';\n\n  Drupal.behaviors.ParagraphLightBox = {\n    attach: function attach(context, settings) {\n      context.querySelectorAll('.paragraphs-types-table tbody tr').forEach(function (item) {\n        try {\n          var bundle = item.querySelector(\"td:nth-child(3)\").textContent;\n          var img = document.createElement('img');\n          img.setAttribute('src', \"\".concat(drupalSettings.path.theme_path, \"assets/paragraphs/\").concat(bundle, \".svg\"));\n          img.setAttribute('alt', '');\n          img.classList.add('paragraph-type-icon');\n          var td = item.querySelector('td:first-child');\n          td.innerHTML = '';\n          td.append(img);\n        } catch (e) {}\n      });\n    }\n  };\n})(jQuery, Drupal);\n\n//# sourceURL=webpack://src/./bundles/paragraphs/js/paragraphs.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./bundles/paragraphs/js/paragraphs.js"]();
/******/ 	
/******/ })()
;