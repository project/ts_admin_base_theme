/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./bundles/commons/js/commons.js":
/*!***************************************!*\
  !*** ./bundles/commons/js/commons.js ***!
  \***************************************/
/***/ (() => {

eval("(function ($, Drupal) {\n  // closure\n\n  // Menu Contribuer : pour avoir le même comportement que celui fourni par le module Toolbar\n  Drupal.behaviors.ts_admin_base_theme_commons = {\n    attach: function attach(context, settings) {\n      // ADD SELECT s2 TO ALL SELECT ELEMENT OF THE BACK-OFFICE\n      $('select', context).not('.select2-hidden-accessible').each(function () {\n        var t = $(this),\n          parents = t.parents('#ckeditor-entity-link-dialog-form');\n        if (parents.length === 0) {\n          t.select2();\n        }\n      });\n    }\n  };\n})(jQuery, Drupal);\n\n//# sourceURL=webpack://src/./bundles/commons/js/commons.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./bundles/commons/js/commons.js"]();
/******/ 	
/******/ })()
;