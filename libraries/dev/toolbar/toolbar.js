/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./bundles/toolbar/js/toolbar.js":
/*!***************************************!*\
  !*** ./bundles/toolbar/js/toolbar.js ***!
  \***************************************/
/***/ (() => {

eval("if (!window.localStorage.getItem('Drupal.toolbar.activeTabID')) {\n  window.localStorage.setItem('Drupal.toolbar.activeTabID', JSON.stringify(\"toolbar-item-administration\"));\n}\n(function ($, Drupal, DrupalSettings) {\n  // closure\n  'use strict';\n\n  Drupal.behaviors.custom_toolbar = {\n    attach: function attach(context, settings) {\n      context.querySelectorAll('.toolbar-menu-administration').forEach(function (item) {\n        item.addEventListener('scroll', function () {\n          item.style.setProperty('--scroll-left', '-' + item.scrollLeft + 'px');\n        });\n      });\n    }\n  };\n})(jQuery, Drupal, drupalSettings);\n\n//# sourceURL=webpack://src/./bundles/toolbar/js/toolbar.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./bundles/toolbar/js/toolbar.js"]();
/******/ 	
/******/ })()
;