(function ($, Drupal) { // closure

  // Menu Contribuer : pour avoir le même comportement que celui fourni par le module Toolbar
  Drupal.behaviors.ts_admin_base_theme_commons = {
    attach: function (context, settings) {

      // ADD SELECT s2 TO ALL SELECT ELEMENT OF THE BACK-OFFICE
      $('select', context).not('.select2-hidden-accessible').each(function () {
        var t = $(this),
          parents = t.parents('#ckeditor-entity-link-dialog-form');

        if (parents.length === 0) {
          t.select2();
        }
      });

    }
  };
}(jQuery, Drupal));
