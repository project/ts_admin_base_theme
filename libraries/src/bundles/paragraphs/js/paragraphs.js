(function ($, Drupal) { // closure
    'use strict';
    Drupal.behaviors.ParagraphLightBox = {
      attach: (context, settings) => {
        context.querySelectorAll('.paragraphs-types-table tbody tr').forEach(item => {
          try{
            const bundle = item.querySelector("td:nth-child(3)").textContent
            const img = document.createElement('img')
            img.setAttribute('src', `${drupalSettings.path.theme_path}assets/paragraphs/${bundle}.svg`)
            img.setAttribute('alt', '')
            img.classList.add('paragraph-type-icon')

            const td = item.querySelector('td:first-child')
              td.innerHTML = ''
              td.append(img)
          }
          catch (e) {

          }
        })
      }
    };
}(jQuery, Drupal));
