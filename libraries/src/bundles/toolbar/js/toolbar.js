if (!window.localStorage.getItem('Drupal.toolbar.activeTabID')) {
  window.localStorage.setItem('Drupal.toolbar.activeTabID', JSON.stringify("toolbar-item-administration"));
}



(function ($, Drupal, DrupalSettings) { // closure
  'use strict';
  Drupal.behaviors.custom_toolbar = {
    attach: (context, settings) => {
      context.querySelectorAll('.toolbar-menu-administration').forEach(item => {
        item.addEventListener('scroll', () => {
          item.style.setProperty('--scroll-left', '-' + item.scrollLeft + 'px');
        })
      })
    }
  };
}(jQuery, Drupal, drupalSettings));