<?php

namespace Drupal\ts_admin_base_theme\Preprocess\Fields;

use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Adds a edition button to the referenced fields.
 *
 * @package Drupal\ts_admin_base_theme\Preprocess\Fields
 */
class EntityReference {

  use StringTranslationTrait;

  /**
   * Singleton.
   *
   * @var \Drupal\ts_admin_base_theme\Preprocess\Fields\EntityReference
   */
  protected static $me;

  /**
   * Retourne le singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    if (!isset(static::$me)) {
      static::$me = new static();
    }

    return static::$me;
  }

  /**
   * Ajoute un lien vers le form d'édition d'une entité référencée.
   *
   * @param array $vars
   *   Les variables.
   */
  public function initEntitiesReferenceField(array &$vars): void {
    // On check si c'est un champ de type référence et qu'il y a une route.
    if (isset($vars['element']['#autocomplete_route_parameters'])
      && $routeParams = $vars['element']['#autocomplete_route_parameters']) {
      // On récupère les parmaètres de la route.
      if (isset($routeParams['target_type']) && $targetType = $routeParams['target_type']) {
        if (isset($vars['element']['#value']) && $value = $vars['element']['#value']) {
          try {
            // ON récupère l'id de l'entité qui est référencée.
            if ($entityId = EntityAutocomplete::extractEntityIdFromAutocompleteInput($value)) {
              // On génère l'url d'édition.
              $url = Url::fromRoute('entity.' . $targetType . '.edit_form',
                                    [$targetType => $entityId],
                                    [
                                      'attributes' => [
                                        'target' => '_blank',
                                        'class' => 'button',
                                      ],
                                    ]);

              // Ajout du lien dans les données de template.
              $vars['edit'] = Link::fromTextAndUrl($this->t('Editer'), $url)
                ->toString();
            }
          }
          catch (\Exception $e) {
            // Mute exception...
          }
        }
      }
    }
  }

}
