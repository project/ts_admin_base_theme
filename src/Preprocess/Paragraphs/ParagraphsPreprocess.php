<?php

namespace Drupal\ts_admin_base_theme\Preprocess\Paragraphs;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\paragraphs\ParagraphsTypeInterface;
use Drupal\ts_dx\Services\Theme\TwigExtension;

/**
 * Preprocess for paragraphs.
 *
 * @package Drupal\ts_admin_base_theme\Preprocess\Paragraphs
 */
class ParagraphsPreprocess {

  /**
   * Singleton.
   *
   * @var static
   */
  protected static $me;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Theme tools.
   *
   * @var \Drupal\ts_dx\Services\Theme\TwigExtension
   */
  protected TwigExtension $twigExtension;

  /**
   * Types cache.
   *
   * @var \Drupal\paragraphs\Entity\ParagraphsType[]
   */
  protected array $paragraphsLabelCache = [];

  /**
   * ParagraphsPreprocess constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\ts_dx\Services\Theme\TwigExtension $twigExtension
   *   The twig extension.
   */
  private function __construct(EntityTypeManagerInterface $entityTypeManager, TwigExtension $twigExtension) {
    $this->entityTypeManager = $entityTypeManager;
    $this->twigExtension = $twigExtension;
  }

  /**
   * The singleton.
   *
   * @return static
   *   The singleton.
   */
  public static function me() {
    if (!isset(static::$me)) {
      static::$me = new static(
        \Drupal::service('entity_type.manager'),
        TwigExtension::instance()
      );
    }

    return static::$me;
  }

  /**
   * Init the paragraph list builder.
   *
   * @param array $variables
   *   The variables.
   */
  public function initParagraphsListBuilder(array &$variables) {
    $keys = ['element', 'label', '#markup'];

    // Get the label.
    $label = NestedArray::getValue($variables, $keys);
    if ($label) {
      $label = trim(strip_tags($label));

      // Add image and title.
      $paragraph_type = $this->getParagraphTypeFromParagraphLabel($label);
      if ($paragraph_type) {
        $variables['attributes']['data-bundle'] = $paragraph_type->id();
        $variables['attributes']['class'][] = 'paragraph-head-wrapper';

        $thumbnail_path = $this->getParagraphTypeThumbnailPath($paragraph_type);

        $variables['children'] = [
          [
            '#type' => 'container',
            '#attributes' => ['class' => ['img-wrapper']],
            0 => [
              '#markup' => $thumbnail_path ? '<img alt="" src="' . $this->twigExtension->getCurrentModulePath('assets/paragraphs/' . $paragraph_type->id() . '.svg') . '">' : '',
            ],
          ],
          [
            '#type' => 'container',
            '#attributes' => ['class' => ['title']],
            0 => [
              '#markup' => $paragraph_type->label(),
            ],
          ],
        ];
      }
    }
  }

  /**
   * Return the type of paragraph from label.
   *
   * @param string $label
   *   The label.
   *
   * @return \Drupal\paragraphs\Entity\ParagraphsType|null
   *   The paragraph type.
   */
  public function getParagraphTypeFromParagraphLabel(string $label) {
    if (!isset($this->paragraphsLabelCache[$label])) {
      $paragraphs_types_definition = $this->entityTypeManager->getDefinition('paragraphs_type');
      if ($paragraphs_types_definition) {
        $storage = $this->entityTypeManager->getStorage('paragraphs_type');
        $label_field = $paragraphs_types_definition->getKey('label');
        $result = $storage->loadByProperties([
          $label_field => $label,
        ]);
        $this->paragraphsLabelCache[$label] = reset($result);
      }
      else {
        $this->paragraphsLabelCache[$label] = NULL;
      }
    }

    return $this->paragraphsLabelCache[$label];
  }

  /**
   * Init pargraphs libraries.
   *
   * @param array $variables
   *   The variables.
   */
  public function initParagraphsList(array &$variables) {
    // Ajout de la libraries.
    $variables['#attached']['library'][] = 'ts_admin_base_theme/paragraphs';
    $variables['#attached']['drupalSettings']['path']['theme_path'] = $this->twigExtension->getCurrentModulePath('');

    $variables['page']['content']['#prefix'] = '<div class="paragraphs-types-table">';
    $variables['page']['content']['#suffix'] = '</div>';
  }

  /**
   * Get the thumbnails image of the paragraph.
   *
   * @param \Drupal\paragraphs\ParagraphsTypeInterface $paragraphs_type
   *   The paragraph type.
   */
  protected function getParagraphTypeThumbnailPath(ParagraphsTypeInterface $paragraphs_type) {
    $path = NULL;
    $root = \Drupal::root();
    $pattern = $root . $this->twigExtension->getCurrentModulePath('assets/paragraphs/' . $paragraphs_type->id() . '.*');
    $files = glob($pattern);
    if ($firstFile = reset($files)) {
      $path = str_replace($root, '', $firstFile);
    }

    return $path;
  }

}
