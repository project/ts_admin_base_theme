# TS Admin Base Theme

## Installation

1. Ajouter le repo
`composer config repositories.ts_admin_base_theme vcs git@git.drupal.org:sandbox/tsecher-3262493.git`

2. Require global
`composer  require drupal/ts_admin_base_theme`

3. Install
`drush then ts_admin_base_theme`


## Principe
Le TS Admin Base Theme a pour but d'ajouter des fonctionnalités
récurrente.
Etant versionné, il fait l'interface entre le theme admin dédié au projet
et le theme admin de base.

### Astuce
Pour changer le theme de base
```
/**
 * Implements hook_system_info_alter().
 */
function mon_module_system_info_alter(array &$info, \Drupal\Core\Extension\Extension $file, $type) {
  if( $type === "theme" ){
    if( $info['name'] === 'TS Base Theme' ){
      $info['base theme']= "seven";
    }
  }
}
```

## Features
### EntityReference Field
Sur chaque champs faisant référence à une entité, le thème ajoute
automatiquement un lien vers le formulaire d'édition de l'entité référencée.

### Paragraphs
Le theme permet de mieux gérer les images de type de paragraphes en back
office.
Par défaut les icones de types de paragraphes sont stockées dans les files,
donc non versionnés.
Grâce au template, il suffit d'ajouter un fichier svg au nom du type de
paragraphe, dans le répertoire  `/assets/paragraphs` du theme actif de back
office pour avoir une image.
Par exemplce, pour un type de paragraphe 'title_text', il suffit d'ajouter
le fichier :
`/web/themes/custom/mon_admin_theme/assets/paragraphs/title_text.svg`.

Pour une meilleur cohérence, il existe des examples dans `/web/themes/contrib/ts_admin_base_theme/assets/paragraphs`
